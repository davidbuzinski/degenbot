var tmi = require('tmi.js');
var fs = require("fs");
const io = require("socket.io-client");

const socket = io("ws://aggregator:5000", {
  reconnectionDelayMax: 10000,
});

var options = JSON.parse(fs.readFileSync("client_secret.json"))

console.log(options);

function isBroadcaster(user,channel) {
  if (('#' + user.username) === channel) {
    return true
  } else {
    return false
  }
}



var client = new tmi.client(options);
client.connect().catch(console.error);
client.on('message', (channel, tags, message, self) => {
  if(self) return;
  socket.emit('chat message', message);
	if(message.toLowerCase() === '!hello') {
		client.say(channel, `@${tags.username}, heya!`);
	}
});