const { OAuthClient } = require('./OAuthClient');
const { get_livechat_id, get_messages, handle_msg } = require('./chat_utils');
const { readFileSync } = require('fs');
const io = require("socket.io-client");

const credentials = JSON.parse(readFileSync('client_secret.json', 'utf8'));

const SCOPES = ['https://www.googleapis.com/auth/youtube'];
const TOKEN_DIR = './';
const TOKEN_PATH = TOKEN_DIR + 'authtoken.json';
const CLIENT_SECRET = credentials.web.client_secret;
const CLIENT_ID = credentials.web.client_id;
const REDIRECT_URL = credentials.web.redirect_uris[0];

var oauth2Client = new OAuthClient(SCOPES, TOKEN_DIR, TOKEN_PATH, CLIENT_SECRET, CLIENT_ID, REDIRECT_URL);
let pageToken;

async function main(id, socket) {
    oauth2Client.request((auth) => {
        get_messages(id, pageToken, auth).then(({messages, nextPageToken}) => {
            pageToken = nextPageToken;
            console.log(messages.length.toString() + " new messages received!");
            messages.map((msg) => {
                handle_msg(msg, socket, auth)
            })
        })
    })
}

const socket = io("ws://aggregator:5000", {
  reconnectionDelayMax: 10000,
});

oauth2Client.request((auth) => {
    get_livechat_id(auth).then((id) => {
        setInterval(() => main(id, socket), 5000)
    })
})