var {google} = require('googleapis');

const handle_msg = (msg, socket, auth) => {
  // write to aggregator
  socket.emit('chat message', msg.snippet.displayMessage);
  if (msg.snippet.displayMessage.startsWith('!hello')) {
    var service = google.youtube('v3');
    service.liveChatMessages.insert({
      auth: auth,
      part: "snippet",
      requestBody: {
        snippet: {
          liveChatId: msg.snippet.liveChatId,
          type: "textMessageEvent",
          textMessageDetails: {
            messageText: "Hello " + msg.authorDetails.displayName + "!"
          },
          authorDetails: {
            displayName: "dgenerate bot"
          }
        }  
      }
    }, (err, response) =>{
      if (err) {
        console.log(err);
      } else {
        // write to aggregator or something
        socket.emit('chat message', response.snippet.displayMessage);
      }
    })
  }
}

const get_livechat_id = (auth) => {
  return new Promise( (resolve, reject) => {
    var service = google.youtube('v3');
    service.liveBroadcasts.list({
      auth: auth,
      broadcastType: "all",
      part: 'snippet, contentDetails, status',
      mine: true
    }, (err, response) => {
      if (err) {
        reject('The API returned an error: ' + err);
      }
      const streams = response.data.items;
      if (streams.length === 0) {
        console.log('There are no active livestreams')
        return ""
      } else {
        const streamMeta = streams[0].snippet
        return resolve(streamMeta.liveChatId)
      }
    })  
  })
}


const get_messages = (liveChatId, pageToken, auth) => {
  return new Promise( (resolve, reject) => {
    var service = google.youtube('v3');
    service.liveChatMessages.list({
      auth: auth,
      liveChatId: liveChatId,
      part: "id, snippet, authorDetails",
      pageToken: pageToken
    }, (err, response) => {
      if (err) {
        reject('Could not receive messages: ' + err);
      } else {
        resolve({messages: response.data.items, nextPageToken: response.data.nextPageToken});
      }
    })
  })
}

exports.get_livechat_id = get_livechat_id
exports.get_messages = get_messages
exports.handle_msg = handle_msg