var fs = require('fs');
var readline = require('readline');
var {google} = require('googleapis');
var OAuth2 = google.auth.OAuth2;

class OAuthClient {
  constructor(SCOPES, TOKEN_DIR, TOKEN_PATH, CLIENT_SECRET, CLIENT_ID, REDIRECT_URL) {
      this.scopes = SCOPES;
      this.token_path = TOKEN_PATH;
      this.token_dir = TOKEN_DIR;
      this.oauth2Client = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
  }

  request = (callback) => {
    fs.readFile(this.token_path, (err, token) => {
      if (err) {
        this.getNewToken(this.oauth2Client, callback);
      } else {
        this.oauth2Client.credentials = JSON.parse(token);
        callback(this.oauth2Client);
      }
    });
  }
    
  getNewToken = (callback) => {
    var authUrl = this.oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: this.scopes
    });
    console.log('Authorize this app by visiting this url: ', authUrl);
    var rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    rl.question('Enter the code from that page here: ', (code) => {
      rl.close();
      this.oauth2Client.getToken(code, (err, token) => {
        if (err) {
          console.log('Error while trying to retrieve access token', err);
          return;
        }
        this.oauth2Client.credentials = token;
        this.storeToken(token);
        callback(this.oauth2Client);
      });
    });
  }
    
  storeToken = (token) => {
    try {
      fs.mkdirSync(this.token_dir);
    } catch (err) {
      if (err.code != 'EEXIST') {
        throw err;
      }
    }
    fs.writeFile(this.token_path, JSON.stringify(token), (err) => {
      if (err) throw err;
      console.log('Token stored to ' + this.token_path);
    });
  }
}

exports.OAuthClient = OAuthClient;