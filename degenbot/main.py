import numpy as np

collect_data = True
train_model = True

if __name__ == '__main__':
    if collect_data == True:
        from data.medium_api import get_tag_sentences
        from data.reddit_api import get_subreddit_content

        subreddits = ['wallstreetbets','poker','weeabootales','waifuism','foreveralone','kotakuinaction','unpopularopinion', 'conspiracy','virginvschad']
        medium_tags = ['careers','fitness','motivation','self-improvement','life-lessons','inspiration','career-advice','leadership','wellness','mindfulness']

        positives = []
        for subreddit in subreddits:
            positives += get_subreddit_content(subreddit)
        negatives = []
        for tag in medium_tags:
            negatives += get_tag_sentences(tag)

        print(len(positives))
        print(len(negatives))

        examples = np.array(list(zip(positives,np.ones((len(positives),)))) + list(zip(negatives,np.zeros((len(negatives),)))))
        np.save("data/trainingExamples/labelledData.npy",examples)

        from model.NNet import embed_data
        sentences = list(examples.T[0])
        labels = list(examples.T[1])
        sequences = embed_data(sentences,30)
        embedded_examples = list(zip(sequences,labels))
        np.save("data/trainingExamples/embeddedData.npy",embedded_examples)

    if train_model == True:
        from model.NNet import build_model, train_model, save_model, load_mdl
        embedded_data = np.load('data/trainingExamples/embeddedData.npy', allow_pickle=True)
        X = [x[0] for x in embedded_data]
        Y = [x[1] for x in embedded_data]
        model = build_model()
        model = train_model(X,Y,model,5)
        save_model(model)
