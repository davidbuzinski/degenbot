from gensim import models
from data.text_utils import remove_unwanted
from keras.models import Sequential, load_model
from keras.layers import *
import tensorflow as tf
import numpy as np 

MAX_SEQUENCE_LENGTH = 30
embedding_dim = 300
embedding_model = models.KeyedVectors.load_word2vec_format('./model/trainedModel/GoogleNews-vectors-negative300.bin.gz', binary=True)
dict_size = len(embedding_model.vocab.keys())

def build_model():
    model = Sequential()
    model.add(Embedding(dict_size,
                            embedding_dim,
                            weights=[embedding_model.vectors],
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=False))
    model.add(LSTM(256, return_sequences=True))
    model.add(LSTM(128))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
    return model

def train_model(X,Y,model,num_epochs):
    """ Trains character recognition model.
        X is dataset (images of size input_shape)
        Y is labels for the dataset (must have num_classes labels)
    """
    # train on dataset X with labels Y for num_epochs epochs
    model.fit(X,Y,batch_size=64,epochs=num_epochs)
    return model

def pred(model,sentence):
    sentence = remove_unwanted(sentence.lower()).split(' ')
    sequence = word2vec(sentence)
    sequence = np.array(pad_or_truncate_sequence(sequence,MAX_SEQUENCE_LENGTH)).reshape((1,30))
    return model.predict(sequence)

def save_model(model,directory = "model/trainedModel/",filename = "degenerator.h5"):
    filepath = directory + filename
    model.save(filepath)
    print("Successfully saved model")

def load_mdl(directory = "model/trainedModel/",filename = "degenerator.h5"):
    filepath = directory + filename
    model = load_model(filepath)
    print("Successfully loaded character model")
    return model



def word2vec(sentence):
    vocab = embedding_model.vocab.keys()
    vectors=[]
    for w in sentence:
        if w in vocab:
            vectors.append(embedding_model.vocab[w].index)
        else:
            print("Word {} not in vocab".format(w))
    return vectors

def pad_or_truncate_sequence(sequence, max_seq_len):
    len_sequence = len(sequence)
    new_sequence = sequence
    if len_sequence < max_seq_len:
        new_sequence = sequence + [0]*(max_seq_len - len_sequence)
    elif len_sequence > max_seq_len:
        new_sequence = sequence = sequence[:max_seq_len]
    return new_sequence

def embed_data(sentences, max_seq_len):
    sequences = []
    for sentence in sentences:
        sequence = word2vec(sentence)
        sequence = pad_or_truncate_sequence(sequence,max_seq_len)
        sequences.append(sequence)
    return sequences
