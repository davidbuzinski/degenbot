from requests import get
from functools import reduce
from data.text_utils import remove_unwanted, remove_special_characters

def get_subreddit_top_posts(subreddit):
    """
    inputs:
        subreddit - a subreddit name as a string
    returns:
        post_urls - a list of the urls of top posts in subreddit
    """
    base_url = "https://www.reddit.com/r/"
    top = "/top.json"
    user_agent = {'User-agent': 'degenbot 1.0'}
    resp = get(base_url + subreddit + top, headers = user_agent)
    if resp.status_code != 200:
        print("WARNING: Could not complete Reddit request. WARNING CODE: " + str(resp.status_code))
        return []
    posts = resp.json().get('data',{}).get('children')
    if not posts:
        print("WARNING: No valid posts returned from Reddit request.")
        return []
    # Map data to text of post
    post_urls = list(map(lambda post: post.get('data',{}).get('url'), posts))
    # Remove posts with no text
    post_urls = list(filter(lambda url: url, post_urls))
    return post_urls

def get_comment_text(comment):
    """
    inputs:
        comment - a dict that expects comment content
                  as a string in comment['data']['body']
    returns:
        valid_comment_text - the comment contend without undesired characters
                             as a list of words
    """
    if not comment:
        return []
    comment_text = comment.get("data",{}).get("body")
    if not comment_text:
        return []
    comment_text = comment_text.lower()
    comment_text = remove_unwanted(comment_text)
    comment_text_list = comment_text.split(".")
    comment_text_list = list(map(lambda text: text.split(" "), comment_text_list))
    valid_comment_text = list(filter(lambda comment: len(comment)>5, comment_text_list))
    return valid_comment_text

def get_post_and_comment_text(post_url):
    user_agent = {'User-agent': 'degenbot 1.0'}
    try:
        resp = get(post_url + "comments.json", headers = user_agent)
        post = resp.json()[0]
        comments =  resp.json()[1]
    except:
        print("WARNING: Invalid response type for "+ post_url +".")
        return []
    comments = comments.get("data",{}).get("children")
    comment_texts = list(map(get_comment_text,comments))
    post_text = post.get("data",{}).get("selftext")
    if post_text:
        post_text = post_text.lower()
        post_text = remove_unwanted(post_text)
        post_text = post_text.split(".")
        post_text = list(map(lambda text: text.split(" "), post_text))
    else:
        post_text = []
    content_text = post_text + comment_texts
    valid_content_text = list(filter(lambda text: text, content_text))
    if not valid_content_text:
        return []
    valid_content_text = list(reduce(lambda s1, s2: s1+s2 , valid_content_text))
    valid_content_text = list(map(remove_special_characters, valid_content_text))
    return valid_content_text

def get_subreddit_content(subreddit):
    post_urls = get_subreddit_top_posts(subreddit)
    content_texts = list(map(get_post_and_comment_text, post_urls))
    content_texts = list(filter(lambda text: text, content_texts))
    if not content_texts:
        print("WARNING: No usable content from r/" + subreddit)
        return []
    content = list(reduce(lambda s1, s2: s1+s2, content_texts))
    return content

if __name__ == "__main__":
    content = get_subreddit_content("wallstreetbets")
    print(content[0])
