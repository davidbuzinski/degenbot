from requests import get
from bs4 import BeautifulSoup
from functools import reduce
from data.text_utils import remove_unwanted, remove_special_characters
from time import sleep

def get_tag_content(tag):
    """
    inputs:
        tag - the tag that you'd like to search on medium
    returns:
        urls - a list of urls for articles returned related to tag
    """
    resp = get("https://medium.com/tag/" + tag)
    if resp.status_code != 200:
        print("WARNING: Could not complete Medium request. WARNING CODE: " + str(resp.status_code))
        return []
    soup = BeautifulSoup(resp.text, features="lxml")
    a_tags = soup.find_all("a", class_="")
    urls = list(map(lambda a: a.attrs.get('href'), a_tags))
    # Remove instances where no href is present
    urls = list(filter(lambda url: url, urls))
    return urls

def get_article_content(url):
    """
    inputs:
        url - url to article with desired content
    returns:
        sentences - sentinces from article content
    """
    resp = get(url)
    if resp.status_code != 200:
        print("WARNING: Could not find Medium article at " + url + ". WARNING CODE: " + str(resp.status_code))
        return []
    soup = BeautifulSoup(resp.text, features="html.parser")
    # Filter non-English articles
    lang = soup.find('html').attrs.get('lang')
    if lang != "en":
        print("Article " + url + " is not in English.")
        return []

    article = soup.find('article')
    paragraphs = article.find_all("p")
    if not paragraphs:
        print("WARNING: No content for article " + url + ".")
        return []
    p_text = list(map(lambda p: p.text, paragraphs))
    p_sentences = list(map(lambda p: p.split("."), p_text))
    # Remove empty sentences and exremely short sentences
    p_sentences = list(filter(lambda sent: sent, p_sentences))
    # We have a list of lists of sentences. Reduce to list of sentences
    sentences = list(reduce(lambda s1, s2: s1 + s2, p_sentences))
    # Format to lower,remove undesired characters, remove very short sentences, and split into word list
    clean_sentences = list(map(lambda sent:remove_unwanted(sent.lower()).split(' '), sentences))
    clean_sentences = list(map(remove_special_characters, clean_sentences))
    valid_sentences = list(filter(lambda sent: len(sent)>4, clean_sentences))
    return valid_sentences

def get_tag_sentences(tag):
    hrefs = get_tag_content(tag)
    article_sentences = []

    # need to do this dumb thing because of api call limits or something?
    for url in hrefs:
        new_content = get_article_content(url)
        if new_content:
            article_sentences += get_article_content(url)
        sleep(2)
    article_sentences = list(filter(lambda sent: sent, article_sentences))
    if not article_sentences:
        print("WARNING: No usable content from medium tag " + tag)
        return []
    # sentences = list(reduce(lambda s1, s2: s1 + s2, article_sentences))
    return article_sentences

if __name__ == "__main__":
    sents = get_tag_sentences('careers')
    print(sents[0])
