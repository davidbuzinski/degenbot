from functools import reduce

def remove_unwanted(text):
    """
    inputs:
        text - a string of text
    outputs:
        clean_text - a string of text without any characters in unwanted
    """
    if not text:
        return ''
    unwanted = ['\n','\t','?','!','@',',','-',':','/']
    # Note that pythons reduce is fold left so our initial text will go in index 0
    clean_text = str(reduce(lambda s1, s2: s1.replace(s2,''), [text] + unwanted))
    return clean_text

def remove_special_characters(word_list):
    clean_word_list = list(filter(lambda word: word.isalnum(), word_list))
    return clean_word_list
