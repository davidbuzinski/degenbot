import numpy as np
import io
from math import ceil
import tensorflow as tf
from flask import Flask, request, jsonify
from model.NNet import load_mdl, pred

model = None
graph = None

def load_model():
    global model
    global graph
    model = load_mdl()
    graph = tf.compat.v1.get_default_graph()


app = Flask(__name__)

@app.route('/predict', methods=["POST"])
def predict():

    data = {'success': False}

    if request.method == 'POST':
        rating = ceil(pred(model, request.data)*100)
        data['prediction'] = rating
        data['success'] = True
    return jsonify(data)


            
if __name__=="__main__":
    print('loading model...')
    load_model()
    print('serving model at port 5000')
    app.run(host='0.0.0.0', port=5000)